package ru.nsu.ccfit.Kartonis.factory;

import ru.nsu.ccfit.Kartonis.factory.details.Accessory;
import ru.nsu.ccfit.Kartonis.factory.details.Body;
import ru.nsu.ccfit.Kartonis.factory.details.Engine;
import ru.nsu.ccfit.Kartonis.factory.staff.Dealer;
import ru.nsu.ccfit.Kartonis.factory.staff.Provider;
import ru.nsu.ccfit.Kartonis.factory.staff.Worker;

import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class Factory implements Runnable
{
    private List<Thread> people = new LinkedList<>();
    private int countWorker = 0, countDealer = 0, countProviders = 0, countStockBodySize = 0,
            countStockMotorSize = 0, countStockAccessorySize = 0, countAuto = 0, number;
    boolean logging;
    public Dealer dealer;
    public Worker worker;
    public Provider<Engine> providerEngine;
    public Provider<Body> providerBody;
    public Provider<Accessory> providerAccessory;
    private Stock<Engine> stockEngine;
    private Stock<Accessory> stockAccessory;
    private Stock<Body> stockBody;
    private Stock<Automobile> stockAutomobile;
    private  Stock<Automobile> stockSoldAuto;
    private AtomicLong timeAccessory;
    private AtomicLong timeEngine;
    private AtomicLong timeBody;
    private AtomicLong timeWorker;
    private AtomicLong timeDealer;
    private AtomicInteger countSaledAuto;

    public Factory()
    {
        countSaledAuto = new AtomicInteger();
        timeAccessory = new AtomicLong();
        timeEngine = new AtomicLong();
        timeBody = new AtomicLong();
        timeWorker = new AtomicLong();
        timeDealer = new AtomicLong();
        number = 0;
        timeAccessory.set(1000);
        timeEngine.set(2000);
        timeBody.set(3000);
        timeDealer.set(4000);
        timeWorker.set(5000);
        FileInputStream file;
        Properties property = new Properties();
        try {
            file = new FileInputStream("/Users/kartonis/IdeaProjects/factory/src/ru/nsu/ccfit/Kartonis/factory/config.properties");
            property.load(file);

            String stringCountWorker = property.getProperty("Workers");
            countWorker = Integer.parseInt(stringCountWorker);
            String stringCountDealer = property.getProperty("Dealers");
            countDealer = Integer.parseInt(stringCountDealer);
            String stringCountProviders= property.getProperty("Providers");
            countProviders = Integer.parseInt(stringCountProviders);
            String stringStockBody = property.getProperty("StockBodySize");
            countStockBodySize = Integer.parseInt(stringStockBody);
            String stringStockMotor = property.getProperty("StockMotorSize");
            countStockMotorSize = Integer.parseInt(stringStockMotor);
            String stringStockAccessory = property.getProperty("StockAccessorySize");
            countStockAccessorySize = Integer.parseInt(stringStockAccessory);
            String stringLogging = property.getProperty("LogSaled");
            logging = Boolean.parseBoolean(stringLogging);
            String stringAuto = property.getProperty("StorageAutoSize");
            countAuto = Integer.parseInt(stringAuto);

            stockAccessory = new Stock<>(countStockAccessorySize, "stockAccessory");
            stockBody = new Stock<>(countStockBodySize, "stockBody");
            stockEngine = new Stock<>(countStockMotorSize, "stockEngine");
            stockAutomobile = new Stock<>(countAuto, "stockAuto");
            stockSoldAuto = new Stock<>(countAuto, "stockSoldAuto");

        } catch (Exception e) {
            System.err.println("Error");
        }
    }

    public void run(){
        Thread thread;
        for (int i = 0; i < countDealer; ++i){
            dealer = new Dealer("Dealer " + i, timeDealer, stockAutomobile, logging, i, countSaledAuto, stockSoldAuto);
            thread = new Thread(dealer);
             people.add(thread);
             thread.start();
        }
        for  (int i = 0; i < countWorker; ++i){
            worker = new Worker("Worker " + i, timeWorker, stockEngine, stockAccessory, stockBody, stockAutomobile);
            thread = new Thread(worker);
            people.add(thread);
            thread.start();

        }
        for (int i = 0; i < countProviders; ++i){
            providerAccessory =  new Provider<>("Provider " + i, timeAccessory, stockAccessory, (id) -> new Accessory(id));
            thread = new Thread(providerAccessory);
            people.add(thread);
            thread.start();
        }
        for (int i = 0; i < countProviders; ++i){
            providerEngine =  new Provider<>("Provider " + i, timeEngine,stockEngine, (id) -> new Engine(id));
            thread = new Thread(providerEngine);
            people.add(thread);
            thread.start();
        }
        for (int i = 0; i < countProviders; ++i){
            providerBody =  new Provider<>("Provider " + i, timeBody,stockBody, (id) -> new Body(id));
            thread = new Thread(providerBody);
            people.add(thread);
            thread.start();
        }

        while (countSaledAuto.get() < countAuto)
            try {
                Thread.sleep(1000);
            }
            catch(InterruptedException e){
                break;
            }


        for (Thread t: people)
        {
            t.interrupt();
        }
    }
    public Stock<Engine> getStockEngine(){
        return stockEngine;
    }
    public Stock<Accessory> getStockAccessory(){
        return stockAccessory;
    }
    public Stock<Body> getStockBody(){
        return stockBody;
    }
    public Stock<Automobile> getStockAuto(){
        return stockAutomobile;
    }
    public Stock<Automobile> getStockSoldAuto(){
        return stockSoldAuto;
    }
    public AtomicLong getTimeAccessory(){return timeAccessory;}
    public AtomicLong getTimeEngine(){return timeEngine;}
    public AtomicLong getTimeBody(){return timeBody;}
    public AtomicLong getTimeWorker(){return timeWorker;}
    public AtomicLong getTimeDealer(){return timeDealer;}





}
