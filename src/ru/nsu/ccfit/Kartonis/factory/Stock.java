package ru.nsu.ccfit.Kartonis.factory;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Stock<T>
{
    private String name;
    private Queue<T> queue;
    private int maxSize;
    private int currSize;
    private int lastID;

    public Stock(int max, String name){
        queue = new ConcurrentLinkedQueue<>();
        maxSize = max;
        this.name = name;

    }

    public synchronized void put(T e)
    {
        if (currSize < maxSize) {
            queue.add(e);
            currSize ++;
        }
        else {
            try {
                wait();
            } catch (InterruptedException ex) {
            }
            queue.add(e);
            currSize++;
        }
        notify();
    }

    public synchronized T get(){
        T currentObject;
        while (currSize == 0) {
            try{
                wait();
            }
            catch (InterruptedException ex){
            }
        }

        currentObject = queue.poll();
        if (currentObject == null){
            int flag = 1;
        }
        currSize--;

        notify();
        System.out.println(currentObject);
        return currentObject;
    }
    public synchronized int getLastID(){
        return lastID++;
    }

    public int getCurrSize(){return currSize;}

    public int getMaxSize(){return maxSize;}
}
