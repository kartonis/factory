package ru.nsu.ccfit.Kartonis.factory.staff;

import ru.nsu.ccfit.Kartonis.factory.Stock;

import java.util.concurrent.atomic.AtomicLong;

public class Provider<T> extends Staff implements Runnable
{
    public interface DetailBuilder<T>
    {
         T buildDetail(int id);
    }

    private DetailBuilder<T> db;
    private Stock<T> stock;

    public Provider(String name, AtomicLong time, Stock<T> stockFromFactory, DetailBuilder<T> db){
        super(name, time);
        stock = stockFromFactory;
        this.db = db;

}
    public void run(){
        while(!Thread.interrupted()) {
            try {
                Thread.sleep(worktime.get());
                stock.put(db.buildDetail(stock.getLastID()));
            } catch (InterruptedException e) {
                break;
            }
        }
    };
}
