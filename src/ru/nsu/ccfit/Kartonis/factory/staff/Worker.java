package ru.nsu.ccfit.Kartonis.factory.staff;

import ru.nsu.ccfit.Kartonis.factory.Automobile;
import ru.nsu.ccfit.Kartonis.factory.Stock;
import ru.nsu.ccfit.Kartonis.factory.details.Accessory;
import ru.nsu.ccfit.Kartonis.factory.details.Body;
import ru.nsu.ccfit.Kartonis.factory.details.Engine;

import java.util.concurrent.atomic.AtomicLong;


public class Worker extends Staff implements Runnable
{
    private Stock<Engine> engineStock;
    private Stock<Accessory> accessoryStock;
    private Stock<Body> bodyStock;
    private Stock<Automobile> automobileStock;

    public Worker(String name, AtomicLong time, Stock<Engine> engineStockFromFactory,
                  Stock<Accessory> accessoryStockFromFactory,
                  Stock<Body> bodyStockFromFactory,
                  Stock<Automobile> automobileStockFromFactory)
    {
        super(name, time);
        engineStock = engineStockFromFactory;
        accessoryStock = accessoryStockFromFactory;
        bodyStock = bodyStockFromFactory;
        automobileStock = automobileStockFromFactory;
    }

    public void run()
    {
        while (!Thread.interrupted()) {
            Accessory a = accessoryStock.get();
            Body b = bodyStock.get();
            Engine engine = engineStock.get();
            try {
                Thread.sleep(worktime.get());
                Automobile auto = new Automobile(automobileStock.getLastID(), b, a, engine);
                automobileStock.put(auto);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
