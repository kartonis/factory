package ru.nsu.ccfit.Kartonis.factory.staff;
import ru.nsu.ccfit.Kartonis.factory.Automobile;
import ru.nsu.ccfit.Kartonis.factory.Stock;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class Dealer extends Staff implements Runnable
{
    private Stock<Automobile> automobileStock;
    private Stock<Automobile> soldCars;
    private Boolean logging;
    private Logger log = Logger.getLogger(Dealer.class.getName());
    private int Number;
    private AtomicInteger countSaledAuto;

    public Dealer(String name, AtomicLong worktime, Stock<Automobile> automobileStockFromFactory,
                  boolean loggingFromFactory, int number, AtomicInteger countSaledAuto, Stock<Automobile> soldCars){

        super(name, worktime);
        this.soldCars = soldCars;
        automobileStock = automobileStockFromFactory;
        logging = loggingFromFactory;
        Number = number;
        this.countSaledAuto = countSaledAuto;
        if (logging)
        {
            try
            {
                FileHandler fileHandler = new FileHandler("SalesLog.log");
                log.addHandler(fileHandler);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void run()
    {
        while (!Thread.interrupted()) {
            try {
                Thread.sleep(worktime.get());
                Automobile auto = automobileStock.get();
                countSaledAuto.incrementAndGet();
                if (logging)
                    log.info("Dealer " + String.valueOf(Number) + " Auto " +
                            String.valueOf(auto.getID()) + "(Body ID:" + auto.getbodyID() +
                            ", Accessory ID: " + auto.getAccessoryID() + ", Engine ID: " + auto.getEngineID());
                soldCars.put(auto);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
