package ru.nsu.ccfit.Kartonis.factory;

import ru.nsu.ccfit.Kartonis.factory.details.Accessory;
import ru.nsu.ccfit.Kartonis.factory.details.Body;
import ru.nsu.ccfit.Kartonis.factory.details.Engine;

public class Automobile
{
    private int ID;
    private int bodyID;
    private int accessoryID;
    private int engineID;

    public Automobile(int id, Body b, Accessory a, Engine e){
        ID = id;
        bodyID = b.getID();
        accessoryID = a.getID();
        engineID = e.getID();
    }

    public int getID(){
        return ID;
}
    public int getbodyID(){
        return bodyID;
    }
    public int getAccessoryID(){
        return accessoryID;
    }
    public int getEngineID(){
        return engineID;
    }
}
