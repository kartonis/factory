package ru.nsu.ccfit.Kartonis;
import javafx.scene.control.ProgressBar;
import ru.nsu.ccfit.Kartonis.factory.Factory;
import ru.nsu.ccfit.Kartonis.factory.Stock;
import ru.nsu.ccfit.Kartonis.factory.details.Accessory;
import ru.nsu.ccfit.Kartonis.factory.details.Engine;
import ru.nsu.ccfit.Kartonis.factory.staff.Dealer;
import ru.nsu.ccfit.Kartonis.factory.staff.Provider;
import ru.nsu.ccfit.Kartonis.factory.staff.Staff;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static java.lang.Thread.sleep;

public class Graphics extends JFrame
{
    private LinkedList<Stock> stocks;
    private Factory factory;
    private JPanel panel;
    private Staff[] people = new Staff[5];
    private JLabel[] labels = new JLabel[5];
    private AtomicLong[] time = new AtomicLong[5];
    private JProgressBar[] progressBars = new JProgressBar[5];
    private JSlider[] sliders = new JSlider[5];


    public Graphics()
    {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(600, 500);
        setLocationRelativeTo(null);
        setTitle("Factory");
        setBackground(Color.WHITE);
        setVisible(true);
        stocks = new LinkedList<>();
        factory = new Factory();
        time[0] = factory.getTimeAccessory();
        time[1] = factory.getTimeBody();
        time[2] = factory.getTimeEngine();
        time[3] = factory.getTimeDealer();
        time[4] = factory.getTimeWorker();
        stocks.add(factory.getStockAccessory());
        stocks.add(factory.getStockBody());
        stocks.add(factory.getStockEngine());
        stocks.add(factory.getStockSoldAuto());
        stocks.add(factory.getStockAuto());
        people[0] = factory.providerAccessory;
        people[1] = factory.providerBody;
        people[2] = factory.providerEngine;
        people[3] = factory.dealer;
        people[4] = factory.worker;

        panel = new JPanel();
        add(panel);
        panel.setLayout(new GridLayout(5,3));

    }

    private void instal(int num){
        panel.add(labels[num]);
        progressBars[num].setMaximum(100);
        progressBars[num].setMinimum(0);
        progressBars[num].setStringPainted(true);
        panel.add(progressBars[num], BorderLayout.WEST);
        sliders[num] = new JSlider(1000, 10000, (int)time[num].get());


        sliders[num].addChangeListener((ChangeEvent event) -> {
            time[num].set(sliders[num].getValue());

        });

        panel.add(sliders[num]);
    }

    private void setValueProgress(JProgressBar progressBar, int i){
        double num = ((double) stocks.get(i).getCurrSize() / stocks.get(i).getMaxSize()) * 100;
        progressBar.setValue((int)num);
    }

    public void update()
    {
        Thread thread = new Thread(factory);
        thread.start();
        double num;

        
        labels[0] = new JLabel("Engine", SwingConstants.CENTER);
        labels[1] = new JLabel("Accessory", SwingConstants.CENTER);
        labels[2] = new JLabel("Body", SwingConstants.CENTER);
        labels[3] = new JLabel("Dealer", SwingConstants.CENTER);
        labels[4] = new JLabel("Worker", SwingConstants.CENTER  );


        for (int i = 0; i < progressBars.length; ++i){
            progressBars[i] = new JProgressBar();
            instal(i);
        }
        revalidate();
        try {
        do{
            sleep(500);
            for (int i = 0; i < progressBars.length; ++i){
                setValueProgress(progressBars[i], i);
            }
        }while(stocks.get(3).getCurrSize() < stocks.get(3).getMaxSize());

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
